db.users.find(
    {
        $or: [{"firstName": {$regex: "S"}},{"lastName": {$regex: "D"}}],
    
    },
    {
        "_id" : 0,
        "firstName": 1,
        "lastName": 1
    }
)


db.users.find(
    {
        $and: [
            {"department": "HR"},
            {"age": {$gte: 70}}
        ]
    }
)


db.users.find(
    {
        $and: [
            {"firstName": {$regex: "E"}},
            {"age": {$lte: 30}}
        ]
    }
)
